INCLUDE=${WATCOM}/h

dosexp.exe: main.c voodoo.c voodoo.h pci.c pci.h
	owcc -bdos4g main.c voodoo.c pci.c -o dosexp.exe
run: dosexp.exe
	dosbox-x -c "mount c ." -c "c:" -c dosexp
