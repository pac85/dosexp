#include "voodoo.h"
#include "pci.h"

#include <stdint.h>
#include <stdio.h>
#include <assert.h>
#include <math.h>

#include <conio.h>
#include <i86.h>

bool find_voodoo(int *o_bus, int *o_slot) {
  union REGS  regs;
  int r, dev_id, vendor_id, bus, slot;

  regs.w.cx = 0;
  regs.w.dx = 0x1850;
  regs.h.bh = 7;
  regs.w.ax = 0xb101;
  r = int386( 0x1A, &regs, &regs );
  printf("int 0x1A %i\n", r);

  for (bus = 0; bus < 100; bus++)
    for (slot = 0; slot < 100; slot++) {
      dev_id = pciConfigReadWord(bus, slot, 0, 0x2);
      if (dev_id == 0xffff)
        continue;
      vendor_id = pciConfigReadWord(bus, slot, 0, 0x0);
      printf("%i:%i dev_id %x vendor_id %x\n", bus, slot, dev_id, vendor_id);
      if (VOODOO_VENDOR_ID == vendor_id && dev_id == VOODOO_DEVICE_ID) {
        printf("found voodoo at bus %i slot %i\n", bus, slot);
        *o_bus = bus;
        *o_slot = slot;
        return true;
      }
    }
  puts("Couldn't find voodoo");
  return false;
}

union init_un {
  struct voodoo_init_enable s;
  uint32_t u;
};

struct voodoo_device init_voodoo() {
  struct voodoo_device ret;
  int bus, slot;
  uint32_t bar0, voodoo_base, creg;
  union init_un data, o_data;

  assert(find_voodoo(&bus, &slot));
  bar0 = find_bar0(bus, slot);
  printf("bar is 0x%x\n", bar0);
  voodoo_base = bar0 & 0xffffff00;

  ret.bus = bus;
  ret.slot = slot;
  ret.register_set_addr = voodoo_base;
  ret.linear_fb_addr = voodoo_base + 0x400000;
  ret.texture_memory_addr = voodoo_base + 0x800000;

  // enable cfg regs write
  data.s.enable_init_reg_w = 1;
  data.s.enable_fifo_w = 1;
  data.s.remap = 0;
  data.s.fbi_snooping0 = 0;
  data.s.fbi_snooping0_matching = 0;
  data.s.fbi_snooping0_rw_matching = 0;
  data.s.fbi_snooping1 = 0;
  data.s.fbi_snooping1_matching = 0;

  pciConfigurationWriteWord(bus, slot, 0, voodoo_pci_command, 1 << 1);
  o_data.u = pciConfigReadDWord(bus, slot, 0, voodoo_pci_init_enable);
  pciConfigurationWriteWord(bus, slot, 0, voodoo_pci_init_enable, data.u | (o_data.u & ~7));

  pciConfigurationWriteWord(bus, slot, 0, voodoo_pci_vclk_disable, 0);

  creg = *VOODOO_REG_PTR(ret, MEM_SET_CHIP_FBI, VOODOO_REG_FBINIT1, uint32_t);
  *VOODOO_REG_PTR(ret, MEM_SET_CHIP_FBI, VOODOO_REG_FBINIT1, uint32_t) = creg & ~(1 << 8); //timing reset
  voodoo_wait_idle(&ret, VOODOO_WAIT_FBI);

  creg = *VOODOO_REG_PTR(ret, MEM_SET_CHIP_FBI, VOODOO_REG_FBINIT0, uint32_t);
  *VOODOO_REG_PTR(ret, MEM_SET_CHIP_FBI, VOODOO_REG_FBINIT0, uint32_t) = (creg & ~(1 << 1 | 1 << 2)) | 1 | 1 << 13; //reset fbi,fifo
  voodoo_wait_idle(&ret, VOODOO_WAIT_ALL);

  creg = *VOODOO_REG_PTR(ret, MEM_SET_CHIP_FBI, VOODOO_REG_FBINIT2, uint32_t);
  *VOODOO_REG_PTR(ret, MEM_SET_CHIP_FBI, VOODOO_REG_FBINIT2, uint32_t) = creg | (1 << 22); //enable dram refresh
  voodoo_wait_idle(&ret, VOODOO_WAIT_ALL);

  //remap so dac is accesible
  data.s.enable_init_reg_w = 0;
  data.s.enable_fifo_w = 0;
  data.s.remap = 1;
  pciConfigurationWriteWord(bus, slot, 0, voodoo_pci_init_enable, data.u);
  //TODO ramdac
  data.s.enable_init_reg_w = 1;
  data.s.enable_fifo_w = 1;
  data.s.remap = 0;
  pciConfigurationWriteWord(bus, slot, 0, voodoo_pci_init_enable, data.u);

  voodoo_set_video_mode(&ret, 640, 480);

  //disable VGA passthrough
  //*VOODOO_REG_PTR(ret, MEM_SET_CHIP_FBI, VOODOO_REG_FBINIT0, uint32_t) = 0;
  /* *VOODOO_REG_PTR(ret, MEM_SET_CHIP_FBI, VOODOO_REG_FBINIT1, uint32_t) = 0; */
  /* *VOODOO_REG_PTR(ret, MEM_SET_CHIP_FBI, VOODOO_REG_FBINIT2, uint32_t) = 0; */
  /* *VOODOO_REG_PTR(ret, MEM_SET_CHIP_FBI, VOODOO_REG_FBINIT3, uint32_t) = 0; */
  /* *VOODOO_REG_PTR(ret, MEM_SET_CHIP_FBI, VOODOO_REG_FBINIT4, uint32_t) = 0; */
  voodoo_wait_idle(&ret, VOODOO_WAIT_ALL);

  pciConfigurationWriteWord(bus, slot, 0, voodoo_pci_vclk_enable, 0);

  return ret;
}

void voodoo_set_video_mode(struct voodoo_device *d, int w, int h) {
  struct voodoo_video_dimentions_reg v = {0};
  struct voodoo_back_porch_reg backporch = {0};
  struct voodoo_hsync_reg hsync = {0};
  struct voodoo_vsync_reg vsync = {0};
  int i;
  v.w = w;
  v.h = h;
  /* printf("video mode"); */
  /* for (i = 0; i < 32; i++) */
  /*   printf("%i", ((*(uint32_t *)&v) >> i )&1); */
  /* puts(""); */
  hsync.hsync_on = 64;
  hsync.hsync_off = 800 - 64;
  vsync.vsync_on = 4;
  vsync.vsync_off = 500 - 4;
  backporch.h = 80;
  backporch.w = 13;
  *VOODOO_REG_PTR(*d, MEM_SET_CHIP_FBI, VOODOO_REG_HSYNC, struct voodoo_hsync_reg) = hsync;
  voodoo_wait_idle(d, VOODOO_WAIT_ALL);
  *VOODOO_REG_PTR(*d, MEM_SET_CHIP_FBI, VOODOO_REG_VSYNC, struct voodoo_vsync_reg) = vsync;
  voodoo_wait_idle(d, VOODOO_WAIT_ALL);
  *VOODOO_REG_PTR(*d, MEM_SET_CHIP_FBI, VOODOO_REG_BACK_PORCH, struct voodoo_back_porch_reg) = backporch;
  voodoo_wait_idle(d, VOODOO_WAIT_ALL);
  *VOODOO_REG_PTR(*d, MEM_SET_CHIP_FBI, VOODOO_REG_VIDEO_DIMENTIONS, struct voodoo_video_dimentions_reg) = v;
  voodoo_wait_idle(d, VOODOO_WAIT_ALL);
  *VOODOO_REG_PTR(*d, MEM_SET_CHIP_FBI, VOODOO_REG_FBINIT1, uint32_t) = 5 << 4 | 1 << 12 | 1 << 13 | 1 << 14 | 1 << 15 | 1 << 16;
  voodoo_wait_idle(d, VOODOO_WAIT_ALL);
}

void voodoo_wait_idle(struct voodoo_device *d, uint8_t wait_flags) {
  volatile struct voodoo_status_reg *status_ptr;
  do {
    status_ptr = VOODOO_REG_PTR(*d, MEM_SET_CHIP_FBI, VOODOO_REG_STATUS, struct voodoo_status_reg);
  } while((bool)status_ptr->fbi_busy && (wait_flags & VOODOO_WAIT_FBI) ||
          (bool)status_ptr->trex_busy && (wait_flags & VOODOO_WAIT_TREX) ||
          (bool)status_ptr->sst1_busy && (wait_flags & VOODOO_WAIT_SST1));
}

void voodoo_write_enable(struct voodoo_device *d) {
  *VOODOO_REG_PTR(*d, MEM_SET_CHIP_FBI, VOODOO_REG_FBZMODE, uint32_t) = 1 << 9;
  voodoo_wait_idle(d, VOODOO_WAIT_ALL);
}

void voodoo_viewport(struct voodoo_device *d, int left, int right, int low, int high) {
  struct voodoo_clipleftright lr;
  struct voodoo_cliplowyhighy lh;

  lr.left = left;
  lr.right = right;
  lh.low = low;
  lh.high = high;

  *VOODOO_REG_PTR(*d, MEM_SET_CHIP_FBI, VOODOO_REG_CLIPLEFTRIGHT, struct voodoo_clipleftright) = lr;
  voodoo_wait_idle(d, VOODOO_WAIT_ALL);
  *VOODOO_REG_PTR(*d, MEM_SET_CHIP_FBI, VOODOO_REG_CLIPLOWYHIGHY, struct voodoo_cliplowyhighy) = lh;
  voodoo_wait_idle(d, VOODOO_WAIT_ALL);
}

void voodoo_set_color0(struct voodoo_device *d, struct voodoo_icolor color) {
  *VOODOO_REG_PTR(*d, MEM_SET_CHIP_FBI, VOODOO_REG_COL0, struct voodoo_icolor) = color;
  voodoo_wait_idle(d, VOODOO_WAIT_ALL);
}

void voodoo_set_color1(struct voodoo_device *d, struct voodoo_icolor color) {
  *VOODOO_REG_PTR(*d, MEM_SET_CHIP_FBI, VOODOO_REG_COL1, struct voodoo_icolor) = color;
  voodoo_wait_idle(d, VOODOO_WAIT_ALL);
}

void voodoo_set_startcolor(struct voodoo_device *d, struct voodoo_vec3 color) {
  *VOODOO_REG_PTR(*d, MEM_SET_CHIP_FBI, VOODOO_REG_FSTARR, float) = color.x;
  voodoo_wait_idle(d, VOODOO_WAIT_ALL);
  *VOODOO_REG_PTR(*d, MEM_SET_CHIP_FBI, VOODOO_REG_FSTARG, float) = color.y;
  voodoo_wait_idle(d, VOODOO_WAIT_ALL);
  *VOODOO_REG_PTR(*d, MEM_SET_CHIP_FBI, VOODOO_REG_FSTARB, float) = color.z;
  voodoo_wait_idle(d, VOODOO_WAIT_ALL);
}

void voodoo_fastfill(struct voodoo_device *d) {
  *VOODOO_REG_PTR(*d, MEM_SET_CHIP_FBI, VOODOO_REG_FASTFILL_CMD, uint32_t) = 1;
  voodoo_wait_idle(d, VOODOO_WAIT_ALL);
}

static struct voodoo_vec3 vec3_min(struct voodoo_vec3 *a, struct voodoo_vec3 *b) {
  struct voodoo_vec3 res;
  res.x = fmin(a->x, b->x);
  res.y = fmin(a->y, b->y);
  res.z = fmin(a->y, b->z);
  return res;
}

static struct voodoo_vec3 vec3_max(struct voodoo_vec3 *a, struct voodoo_vec3 *b) {
  struct voodoo_vec3 res;
  res.x = fmax(a->x, b->x);
  res.y = fmax(a->y, b->y);
  res.z = fmax(a->y, b->z);
  return res;
}

void voodoo_triangle(struct voodoo_device *d, struct voodoo_vec3 verts[3], struct voodoo_vec3 colors[3]) {
  struct voodoo_vec3 stemp, bbox[2];
  float dxab, dybc, dxbc, dyab, area;
  int i, j;
  bbox[0] = bbox[1] = verts[0];
  //sort by y
  for (i = 0; i < 3; i++) {
    bbox[0] = vec3_min(&bbox[0], &verts[i]);
    bbox[1] = vec3_max(&bbox[1], &verts[i]);
    for (j = 0; j < i; j++) {
      if (verts[i].y < verts[j].y) {
        stemp = verts[i];
        verts[i] = verts[j];
        verts[j] = stemp;
      }
    }
  }

  dxab = verts[0].x - verts[1].x;
  dyab = verts[0].y - verts[1].y;
  dxbc = verts[1].x - verts[2].x;
  dybc = verts[1].y - verts[2].y;

  area = ((dxab * dybc) - (dxbc * dyab)) / 2;

  *VOODOO_REG_PTR(*d, MEM_SET_CHIP_FBI, VOODOO_REG_FVERTEXAX, float) = verts[0].x;
  voodoo_wait_idle(d, VOODOO_WAIT_ALL);
  *VOODOO_REG_PTR(*d, MEM_SET_CHIP_FBI, VOODOO_REG_FVERTEXAY, float) = verts[0].y;
  voodoo_wait_idle(d, VOODOO_WAIT_ALL);
  *VOODOO_REG_PTR(*d, MEM_SET_CHIP_FBI, VOODOO_REG_FVERTEXBX, float) = verts[1].x;
  voodoo_wait_idle(d, VOODOO_WAIT_ALL);
  *VOODOO_REG_PTR(*d, MEM_SET_CHIP_FBI, VOODOO_REG_FVERTEXBY, float) = verts[1].y;
  voodoo_wait_idle(d, VOODOO_WAIT_ALL);
  *VOODOO_REG_PTR(*d, MEM_SET_CHIP_FBI, VOODOO_REG_FVERTEXCX, float) = verts[2].x;
  voodoo_wait_idle(d, VOODOO_WAIT_ALL);
  *VOODOO_REG_PTR(*d, MEM_SET_CHIP_FBI, VOODOO_REG_FVERTEXCY, float) = verts[2].y;
  voodoo_wait_idle(d, VOODOO_WAIT_ALL);

  *VOODOO_REG_PTR(*d, MEM_SET_CHIP_FBI, VOODOO_REG_FSTARR, float) = 255.0;
  voodoo_wait_idle(d, VOODOO_WAIT_ALL);
  *VOODOO_REG_PTR(*d, MEM_SET_CHIP_FBI, VOODOO_REG_FSTARG, float) = 255.0;
  voodoo_wait_idle(d, VOODOO_WAIT_ALL);
  *VOODOO_REG_PTR(*d, MEM_SET_CHIP_FBI, VOODOO_REG_FSTARB, float) = 0.0;
  voodoo_wait_idle(d, VOODOO_WAIT_ALL);


  *VOODOO_REG_PTR(*d, MEM_SET_CHIP_FBI, VOODOO_REG_FDRDX, float) = -450.0;
  voodoo_wait_idle(d, VOODOO_WAIT_ALL);
  *VOODOO_REG_PTR(*d, MEM_SET_CHIP_FBI, VOODOO_REG_FDRDY, float) = -400.0;
  voodoo_wait_idle(d, VOODOO_WAIT_ALL);

  *VOODOO_REG_PTR(*d, MEM_SET_CHIP_FBI, VOODOO_REG_FTRIANGLE_CMD, float) = area;
  voodoo_wait_idle(d, VOODOO_WAIT_ALL);
}

void vooodoo_dac_write(struct voodoo_device *d, uint8_t reg, uint8_t val) {
  reg &= 0x07;
  *VOODOO_REG_PTR(*d, MEM_SET_CHIP_FBI, VOODOO_REG_DACDATA, uint32_t) = (uint32_t)(reg << 8) | (uint32_t) val;
}

uint8_t vooodoo_dac_read(struct voodoo_device *d, uint8_t reg) {
  reg &= 0x07;
  *VOODOO_REG_PTR(*d, MEM_SET_CHIP_FBI, VOODOO_REG_DACDATA, uint32_t) = (uint32_t)(reg << 8) | (uint32_t) VOODOO_DAC_READ_CMD;
  voodoo_wait_idle(d, VOODOO_WAIT_FBI);

  return (uint32_t)*VOODOO_REG_PTR(*d, MEM_SET_CHIP_FBI, VOODOO_REG_DACREAD, uint32_t);
}

void voodoo_swapbuffers(struct voodoo_device *d, bool vsync) {
  uint32_t temp;
  bool front;
  *VOODOO_REG_PTR(*d, MEM_SET_CHIP_FBI, VOODOO_REG_SWAPBUFFER_CMD, uint32_t) = 0;
  voodoo_wait_idle(d, VOODOO_WAIT_ALL);
  /* temp = *VOODOO_REG_PTR(*d, MEM_SET_CHIP_FBI, VOODOO_REG_FBZMODE, uint32_t); */
  /* front = (temp >> 14) & 1; */
  /* front = !!front; */
  /* *VOODOO_REG_PTR(*d, MEM_SET_CHIP_FBI, VOODOO_REG_FBZMODE, uint32_t) = temp & ~(3 << 14) | temp << 14; */
}

uint32_t voodoo_make_register_address() {
}

void voodoo_print_status(struct voodoo_device *d) {
  volatile struct voodoo_status_reg *status_ptr;
  uint32_t vretrace;
  int i, j;
  while(1) {
    //voodoo_set_video_mode(d, 800, 600);
    status_ptr = VOODOO_REG_PTR(*d, MEM_SET_CHIP_FBI, VOODOO_REG_STATUS, struct voodoo_status_reg);
    printf("   voodoo status register   \n");
    printf("fifo_status %i\n", status_ptr->fifo_freespace);
    printf("vertical retrace %i\n", status_ptr->vertical_retrace);
    printf("fbi busy %i\n", status_ptr->fbi_busy);
    printf("whole register %i\n", *(volatile uint32_t *)status_ptr);
    vretrace = *VOODOO_REG_PTR(*d, MEM_SET_CHIP_FBI, VOODOO_REG_VRETRACE, uint32_t);
    voodoo_wait_idle(d, VOODOO_WAIT_ALL);
    printf("vretrace %u\n", vretrace);
    for (i = 0;i<10000;i++)
      for (j = 0;j<1000;j++)
      {}
    break;
  }
}
