#include "pci.h"

#include <assert.h>

#include <conio.h>
#include <i86.h>
#include <stdint.h>

static uint32_t pci_congig_addr(uint8_t bus, uint8_t slot, uint8_t func, uint8_t offset)
{
  uint32_t lbus  = (uint32_t)bus;
  uint32_t lslot = (uint32_t)slot;
  uint32_t lfunc = (uint32_t)func;

  // Create configuration address as per Figure 1
  return (uint32_t)((lbus << 16) | (lslot << 11) |
                    (lfunc << 8) | (offset & 0xFF) | ((uint32_t)0x80000000));
}

uint16_t pciConfigReadWord(uint8_t bus, uint8_t slot, uint8_t func, uint8_t offset) {
  uint16_t tmp = 0;
  uint32_t address = pci_congig_addr(bus, slot, func, offset & 0xFC);

  // Write out the address
  outpd(0xCF8, address);
  // Read in the data
  // (offset & 2) * 8) = 0 will choose the first word of the 32-bit register
  tmp = (uint16_t)((inpd(0xCFC) >> ((offset & 2) * 8)) & 0xFFFF);
  return tmp;
}

uint32_t pciConfigReadDWord(uint8_t bus, uint8_t slot, uint8_t func, uint8_t offset) {
  uint32_t res;

  res = (uint32_t)pciConfigReadWord(bus, slot, func, offset);
  res |= (uint32_t)pciConfigReadWord(bus, slot, func, offset + 0x2) << 16;

  return res;
}

void
pciConfigurationWriteWord(uint8_t bus,
                          uint8_t slot,
                          uint8_t func,
                          uint8_t offset,
                          uint32_t data)
{
  uint32_t address = pci_congig_addr(bus, slot, func, offset & 0xFC);

  // Write out the address
  outpd(0xCF8, address);
  // Write out data
  outpd(0xCFC, data);
}

uint32_t find_bar0(int bus, int slot) {
  int header_type;
  uint32_t bar;
  header_type = pciConfigReadWord(bus, slot, 0, 0xe) & 0xff;
  assert(header_type == 0);
  bar = pciConfigReadDWord(bus, slot, 0, 0x10);
  return bar;
}
