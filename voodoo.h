#ifndef VOODOO_H_
#define VOODOO_H_

#include <stdint.h>
#include <stdbool.h>

#define VOODOO_VENDOR_ID 0x0121a
#define VOODOO_DEVICE_ID 0x1

#define MEM_SET_CHIP_ALL 0
#define MEM_SET_CHIP_FBI 1
#define MEM_SET_CHIP_TREX0 2
#define MEM_SET_CHIP_TREX1 4
#define MEM_SET_CHIP_TREX2 8

#define VOODOO_REG_PTR(voodoo, chip, reg_addr, t)\
  ((volatile t *)((voodoo).register_set_addr + reg_addr + ((chip) << 10)))

#define VOODOO_REG_STATUS 0x0
#define VOODOO_REG_FVERTEXAX 0x088
#define VOODOO_REG_FVERTEXAY 0x08c
#define VOODOO_REG_FVERTEXBX 0x090
#define VOODOO_REG_FVERTEXBY 0x094
#define VOODOO_REG_FVERTEXCX 0x098
#define VOODOO_REG_FVERTEXCY 0x09c
#define VOODOO_REG_FSTARR 0x0a0
#define VOODOO_REG_FSTARG 0x0a4
#define VOODOO_REG_FSTARB 0x0a8
#define VOODOO_REG_FSTARZ 0x0ac
#define VOODOO_REG_FSTARA 0x0b0
#define VOODOO_REG_FSTARS 0x0b4
#define VOODOO_REG_FSTART 0x0b8
#define VOODOO_REG_FSTARW 0x0bc
#define VOODOO_REG_FDRDX 0x0c0
#define VOODOO_REG_FDGDX 0x0c4
#define VOODOO_REG_FDBDX 0x0c8
#define VOODOO_REG_FDZDX 0x0cc
#define VOODOO_REG_FDADX 0x0d0
#define VOODOO_REG_FDSDX 0x0d4
#define VOODOO_REG_FDTDX 0x0d8
#define VOODOO_REG_FDWDX 0x0dc
#define VOODOO_REG_FDRDY 0x0e0
#define VOODOO_REG_FDGDY 0x0e4
#define VOODOO_REG_FDBDY 0x0e8
#define VOODOO_REG_FDZDY 0x0ec
#define VOODOO_REG_FDADY 0x0f0
#define VOODOO_REG_FDSDY 0x0f4
#define VOODOO_REG_FDTDY 0x0f8
#define VOODOO_REG_FDWDY 0x0fc
#define VOODOO_REG_FTRIANGLE_CMD 0x100
#define VOODOO_REG_FBZMODE 0x110
#define VOODOO_REG_LFBMODE 0x114
#define VOODOO_REG_CLIPLEFTRIGHT 0x118
#define VOODOO_REG_CLIPLOWYHIGHY 0x11c
#define VOODOO_REG_NOP_CMD 0x120
#define VOODOO_REG_FASTFILL_CMD 0x124
#define VOODOO_REG_SWAPBUFFER_CMD 0x128
#define VOODOO_REG_COL0 0x144
#define VOODOO_REG_COL1 0x148
#define VOODOO_REG_VRETRACE 0x204
#define VOODOO_REG_FBINIT0 0x210
#define VOODOO_REG_FBINIT1 0x214
#define VOODOO_REG_FBINIT2 0x218
#define VOODOO_REG_FBINIT3 0x21c
#define VOODOO_REG_FBINIT4 0x200
#define VOODOO_REG_BACK_PORCH 0x208
#define VOODOO_REG_VIDEO_DIMENTIONS 0x20c
#define VOODOO_REG_HSYNC 0x220
#define VOODOO_REG_VSYNC 0x224
#define VOODOO_REG_DACDATA 0x22c
#define VOODOO_REG_DACREAD VOODOO_REG_FBINIT2

#define voodoo_pci_command 0x4
#define voodoo_pci_init_enable 0x40
#define voodoo_pci_vclk_enable 0xc0
#define voodoo_pci_vclk_disable 0xe0

#define VOODOO_WAIT_FBI 1
#define VOODOO_WAIT_TREX 2
#define VOODOO_WAIT_SST1 4
#define VOODOO_WAIT_ALL (VOODOO_WAIT_FBI | VOODOO_WAIT_TREX | VOODOO_WAIT_SST1)

#define VOODOO_DAC_READ_CMD (1 << 11)

struct voodoo_device {
  uint8_t bus, slot;
  uint32_t register_set_addr,
           linear_fb_addr,
           texture_memory_addr;
};

struct voodoo_status_reg {
  unsigned  fifo_freespace: 6;
  unsigned  vertical_retrace: 1;
  unsigned  fbi_busy: 1;
  unsigned  trex_busy: 1;
  unsigned  sst1_busy: 1;
  unsigned  dipslayed_buffer: 2;
  unsigned  memory_fifo_freespace: 16;
  unsigned  swap_pending: 3;
  unsigned  pci_int: 1;
};

struct voodoo_init_enable {
  unsigned  enable_init_reg_w: 1;
  unsigned  enable_fifo_w: 1;
  unsigned  remap: 1;
  unsigned  res1: 1;
  unsigned  fbi_snooping0: 1;
  unsigned  fbi_snooping0_matching: 1;
  unsigned  fbi_snooping0_rw_matching: 1;
  unsigned  fbi_snooping1: 1;
  unsigned  fbi_snooping1_matching: 1;
};

struct voodoo_fbinit0_reg {
  unsigned  passthrough_ctl: 1;
  unsigned  fbi_reset: 1;
  unsigned  fifo_reset: 1;
  unsigned  byte_swizzle: 1;
  unsigned  stall_pci_high_watermark: 1;
  unsigned  res1: 1;
  unsigned  pci_fifo_empty_entries_low_watermark: 5;
  unsigned  stall_pci_low_watermark: 1;
  unsigned  fb_mem: 1;
  unsigned  texture_mem: 1;
  unsigned  fifo_mem: 1;
  unsigned  pci_fifo_empty_entries_high_watermark: 11;
};

struct voodoo_back_porch_reg {
  unsigned  h: 8;
  unsigned  res1: 8;
  unsigned  w: 8;
};

struct voodoo_video_dimentions_reg {
  unsigned  w: 10;
  unsigned  res1: 6;
  unsigned  h: 10;
};

struct voodoo_hsync_reg {
  unsigned hsync_on: 8;
  unsigned res1: 8;
  unsigned hsync_off: 10;
};

struct voodoo_vsync_reg {
  unsigned  vsync_on: 12;
  unsigned  res1: 4;
  unsigned  vsync_off: 12;
};

struct voodoo_vec2 {
  float x, y;
};

struct voodoo_vec3 {
  float x, y, z;
};

struct voodoo_icolor {
  unsigned b: 8;
  unsigned g: 8;
  unsigned r: 8;
  unsigned a: 8;
};

struct voodoo_clipleftright {
  unsigned right: 10;
  unsigned res1: 6;
  unsigned left: 10;
  unsigned res2: 6;
};

struct voodoo_cliplowyhighy {
  unsigned high: 10;
  unsigned res1: 6;
  unsigned low: 10;
  unsigned res2: 6;
};

bool find_voodoo(int *o_bus, int *o_slot);
struct voodoo_device init_voodoo();
void voodoo_set_video_mode(struct voodoo_device *d, int w, int h);
void voodoo_wait_idle(struct voodoo_device *d, uint8_t wait_flags);

void voodoo_write_enable(struct voodoo_device *d);
void voodoo_viewport(struct voodoo_device *d, int left, int right, int low, int high);
void voodoo_set_color0(struct voodoo_device *d, struct voodoo_icolor color);
void voodoo_set_color1(struct voodoo_device *d, struct voodoo_icolor color);
void voodoo_set_startcolor(struct voodoo_device *d, struct voodoo_vec3 color);
void voodoo_fastfill(struct voodoo_device *d);
void voodoo_triangle(struct voodoo_device *d, struct voodoo_vec3 verts[3], struct voodoo_vec3 colors[3]);
void voodoo_swapbuffers(struct voodoo_device *d, bool vsync);

void vooodoo_dac_write(struct voodoo_device *d, uint8_t reg, uint8_t val);
uint8_t vooodoo_dac_read(struct voodoo_device *d, uint8_t reg);

uint32_t voodoo_make_register_address();
void voodoo_print_status(struct voodoo_device *d);

#endif // VOODOO_H_
