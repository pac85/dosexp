#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <stdlib.h>
#include <assert.h>

#include "voodoo.h"

#include <math.h>

int main () {
  struct voodoo_device voodoo;
  struct voodoo_vec3 tri[3], colors[3];
  struct voodoo_icolor clear_color = {0};
  float t = 0.0;
  clear_color.r = 55;
  clear_color.g = 55;
  clear_color.b = 55;
  clear_color.a = 255;
  voodoo = init_voodoo();
  voodoo_print_status(&voodoo);
  voodoo_set_color1(&voodoo, clear_color);
  voodoo_viewport(&voodoo, 0, 640, 0, 480);
  while(1) {
    t += 0.01;
    tri[0].x = 50 + sin(t)*10.0;
    tri[0].y = 50;
    tri[1].x = 400;
    tri[1].y = 50;
    tri[2].x = 50;
    tri[2].y = 400;
    voodoo_write_enable(&voodoo);
    voodoo_fastfill(&voodoo);
    voodoo_triangle(&voodoo, tri, colors);
    voodoo_swapbuffers(&voodoo, false);
  }
}
