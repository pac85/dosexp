#ifndef PCI_H_
#define PCI_H_

#include <stdint.h>

uint16_t pciConfigReadWord(uint8_t bus, uint8_t slot, uint8_t func, uint8_t offset);
uint32_t pciConfigReadDWord(uint8_t bus, uint8_t slot, uint8_t func, uint8_t offset);
void
pciConfigurationWriteWord(uint8_t bus,
                          uint8_t slot,
                          uint8_t func,
                          uint8_t offset,
                          uint32_t data);
uint32_t find_bar0(int bus, int slot);

#endif // PCI_H_
